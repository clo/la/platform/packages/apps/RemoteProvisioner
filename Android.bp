//
// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
package {
    default_applicable_licenses: ["Android-Apache-2.0"],
}

genrule {
    name: "statslog-remoteprovisioner-java-gen",
    tools: ["stats-log-api-gen"],
    cmd: "$(location stats-log-api-gen) --java $(out) --module remoteprovisioner " +
         "--javaPackage com.android.remoteprovisioner --javaClass RemoteProvisionerStatsLog",
    out: ["com/android/remoteprovisioner/RemoteProvisionerStatsLog.java"],
}

android_app {
    name: "RemoteProvisioner",
    platform_apis: true,
    privileged: true,
    system_ext_specific: true,
    defaults: [
        "keymint_use_latest_hal_aidl_ndk_static",
        "keystore2_use_latest_aidl_java",
    ],
    libs: [
        "framework-annotations-lib",
    ],
    optimize: {
        proguard_flags_files: ["proguard.flags"]
    },
    static_libs: [
        "android.hardware.security.rkp-V2-java",
        "android.security.remoteprovisioning-java",
        "androidx.work_work-runtime",
        "cbor-java",
    ],
    resource_dirs: ["res"],
    srcs: [
        ":statslog-remoteprovisioner-java-gen",
        "src/**/*.java"
    ],
}
